package ru.psu.mobileapp.kotlin.lab31realmdatabase.activity

import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.RecyclerView

/********************************************************************************************************
 * Класс отвечает за обработку жестов пользователя в списке.                                            *
 * Селетков Илья, 2018 0301.                                                                            *
 *******************************************************************************************************/
class CItemTouchHelper(listener: ItemTouchListener?)
: ItemTouchHelper.Callback()
{
    private var mListener: ItemTouchListener? = listener

    /****************************************************************************************************
     * Возвращает возможность делать свайп над элементом.                                               *
     ***************************************************************************************************/
    override fun isItemViewSwipeEnabled(): Boolean
    {
        return true
    }
    /****************************************************************************************************
     * Возвращает доступные для обработки направления жестов.                                           *
     ***************************************************************************************************/
    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int
    {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
    }

    /****************************************************************************************************
     * Обработка перемещения элемента.                                                                  *
     ***************************************************************************************************/
    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
               target: RecyclerView.ViewHolder): Boolean
    {
        mListener?.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }
    /****************************************************************************************************
     * Обработка свайпа над элементом.                                                                  *
     ***************************************************************************************************/
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int)
    {
        mListener?.onItemDismiss(viewHolder.adapterPosition)
    }

    /****************************************************************************************************
     * Интерфейс позволит делать обработчики действий пользователя в списке.                            *
     ***************************************************************************************************/
    interface ItemTouchListener
    {
        /************************************************************************************************
         * Перемещение элемента в списке.                                                               *
         ***********************************************************************************************/
        fun onItemMove(fromPosition: Int, toPosition: Int)
        /************************************************************************************************
         * Удаление элемента.                                                                           *
         ***********************************************************************************************/
        fun onItemDismiss(position: Int)
    }
}