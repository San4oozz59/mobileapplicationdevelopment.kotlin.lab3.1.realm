package ru.psu.mobileapp.kotlin.lab31realmdatabase.activity

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import io.realm.RealmRecyclerViewAdapter
import ru.psu.mobileapp.kotlin.lab31realmdatabase.model.CMessage
import android.view.LayoutInflater
import android.widget.TextView
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main.*
import ru.psu.mobileapp.kotlin.lab31realmdatabase.R
import ru.psu.mobileapp.kotlin.lab31realmdatabase.util.CApplication
import java.util.*


/********************************************************************************************************
 * Адаптер для обработки действий в списке сообщений.                                                   *
 * Селетков Илья, 2018 0222.                                                                            *
 *******************************************************************************************************/
class CRecyclerViewAdapterMessages(messages : RealmResults<CMessage>?, realm : Realm, autoUpdate : Boolean)
    : RealmRecyclerViewAdapter<CMessage, CRecyclerViewAdapterMessages.ViewHolder>(messages, autoUpdate, true),

        //Реализация интерфейса для обработки жестов пользователя над списком.
        CItemTouchHelper.ItemTouchListener
{
    private val mRealm                      = realm
    /****************************************************************************************************
     * Конструктор.                                                                                     *
     ***************************************************************************************************/
    init
    {

    }
    /****************************************************************************************************
     * Создание графических компонент для одной строки.                                                 *
     ***************************************************************************************************/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val itemView                        = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_item_messages, parent, false)
        return ViewHolder(itemView)
    }
    /****************************************************************************************************
     * Назначение записи о сообщении на вывод в конкретную визуальную строчку.                          *
     ***************************************************************************************************/
    override fun onBindViewHolder(holder: ViewHolder?, position: Int)
    {
        val message : CMessage?             = getItem(position)
        holder?.message                     = message
    }

    /****************************************************************************************************
     * Перемещение элемента в списке.                                                                   *
     ***************************************************************************************************/
    override fun onItemMove(fromPosition: Int, toPosition: Int)
    {
        return
    }
    /****************************************************************************************************
     * Удаление элемента.                                                                               *
     ***************************************************************************************************/
    override fun onItemDismiss(position: Int)
    {
        val message                         = getItem(position)
        message ?: return

        //Магия однострочников.
        val transaction                     = Realm.Transaction { _ -> message.deleteFromRealm(); }
        mRealm.executeTransaction(transaction)
        return
    }
    /****************************************************************************************************
     * Класс для хранения графических элементов из одной строчки.                                       *
     ***************************************************************************************************/
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        private var mTextViewText : TextView?
                                            = null
        private var mTextViewDate : TextView?
                                            = null
        var message : CMessage?             = null
            set(message)
            {
                field                       = message
                //Выводим содержимое записи о сообщениии в визуальные компоненты.
                mTextViewText?.text         = message?.text
                mTextViewDate?.text         = CApplication.Formatter.format(message?.date)
            }
        init
        {
            mTextViewText                   = itemView.findViewById(R.id.TextViewText)
            mTextViewDate                   = itemView.findViewById(R.id.TextViewDate)
        }
    }

}