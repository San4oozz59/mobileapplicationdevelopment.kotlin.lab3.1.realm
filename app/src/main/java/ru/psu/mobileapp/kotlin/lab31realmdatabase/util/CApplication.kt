package ru.psu.mobileapp.kotlin.lab31realmdatabase.util

import android.app.Application
import io.realm.Realm;
import io.realm.RealmConfiguration;
import java.text.SimpleDateFormat
import java.util.*


/********************************************************************************************************
 * Основной класс приложения.                                                                           *
 * Селетков Илья, 2018 0222.                                                                            *
 *******************************************************************************************************/
class CApplication : Application()
{
    companion object
    {
        val Formatter : SimpleDateFormat    = SimpleDateFormat("YYYY-MM-DD HH:mm", Locale("ru-RU"))
    }

    /****************************************************************************************************
     * Инициализация данных приложения.                                                                 *
     ***************************************************************************************************/
    override fun onCreate()
    {
        super.onCreate()
        Realm.init(this)
        val config                          = RealmConfiguration.Builder().name("myrealm.realm").build()
        Realm.setDefaultConfiguration(config)
        return
    }
}