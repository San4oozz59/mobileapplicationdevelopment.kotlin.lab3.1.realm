package ru.psu.mobileapp.kotlin.lab31realmdatabase.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import ru.psu.mobileapp.kotlin.lab31realmdatabase.R
import ru.psu.mobileapp.kotlin.lab31realmdatabase.model.CMessage
import java.util.*
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.widget.Toast


/********************************************************************************************************
 * Класс отвечает за обработку действий пользователя в основной активности приложения.                  *
 * Селетков Илья, 2018 0222.                                                                            *
 *******************************************************************************************************/
class CActivityMain : AppCompatActivity() {

    //Объект БД.
    var realm : Realm?                      = null
    //Объект для управления содержимым списка.
    var adapter : CRecyclerViewAdapterMessages?
                                            = null
    /****************************************************************************************************
     * Создание формы.                                                                                  *
     ***************************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        realm                               = Realm.getDefaultInstance()

        realm ?: Toast.makeText(this, getString(R.string.CannotGetRealmInstance), Toast.LENGTH_LONG).show()
        setUpRecyclerView()
        return
    }
    /****************************************************************************************************
     * Задание параметров компонента RecyclerView                                                       *
     ***************************************************************************************************/
    private fun setUpRecyclerView()
    {
        adapter                             = CRecyclerViewAdapterMessages(realm?.where(CMessage::class.java)?.findAll(), realm!!, true)
        RecyclerViewItems.layoutManager     = LinearLayoutManager(this)
        RecyclerViewItems.adapter           = adapter
        RecyclerViewItems.setHasFixedSize(true)
        RecyclerViewItems.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        val callback                        = CItemTouchHelper(adapter)
        val touchHelper                     = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(RecyclerViewItems)
        return
    }
    /****************************************************************************************************
     * Обработка нажатия на кнопку "отправить".                                                         *
     ***************************************************************************************************/
    fun onButtonSendClick(view : View)
    {
        val message                         = CMessage(EditTextMessage.text.toString(), Date())

        //Магия однострочников.
        val transaction                     = Realm.Transaction {bgRealm -> bgRealm.copyToRealm(message); }
        realm?.executeTransactionAsync(transaction)

        //Обновляет визуальные элементы автоматически, не надо специально указывать.
        //adapter?.notifyDataSetChanged()

        EditTextMessage.setText("")
        return
    }
    /****************************************************************************************************
     * Закрытие формы.                                                                                  *
     ***************************************************************************************************/
    override fun onDestroy()
    {
        super.onDestroy()
        RecyclerViewItems?.adapter          = null
        realm?.close()
    }
    /****************************************************************************************************
     * Создание меню.                                                                                  *
     ***************************************************************************************************/
    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        return true
    }

}
